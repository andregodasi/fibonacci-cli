
exports.fibonacci = function(n) {
  applyValidations(n)

  if (n < 3) return n - 1;
  let sequence = [];
  let result = 0;
  for (let i = 0; i < n; i++) {
      if (i < 2) {
          sequence.push(i);
      } else {
          result = sequence[i -1] + sequence[i - 2];
          sequence.push(result);
      }
  }
  return result;
}

function applyValidations(n) {
  validateNullOrUndefined(n)
  validateZero(n)
  validateNegative(n)
  validateNotANumber(n)
  validateFormat(n)
}

function validateNullOrUndefined(n) {
  if (n == null || n == undefined) { throw Error('[n] can\'t be null or undefined') }
}

function validateZero(n) {
  if (n == 0) { throw Error('[n] must be bigger then zero') }
}

function validateNegative(n) {
  if (n < 0) { throw Error('[n] can\'t be negative') }
}

function validateNotANumber(n) {
  if (isNaN(n)) { throw Error('[n] should be a number') }
}

function validateFormat(n) {
  var regex = /^\d+\.\d{0,2}$/;  // NOTE: regex used to match integer numbers.
  if (regex.test(n)) { throw Error('[n] should be an integer') }
}

